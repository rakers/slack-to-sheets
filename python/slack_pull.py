import logging
import os
import json
import jq
# Import WebClient from Python SDK (github.com/slackapi/python-slack-sdk)
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

# WebClient instantiates a client that can call API methods
# When using Bolt, you can use either `app.client` or the `client` passed to listeners.
#client = WebClient(token=os.environ.get("SLACK_BOT_TOKEN"))
client = WebClient(token="<SLACK BOT TOKEN>")
logger = logging.getLogger(__name__)
# Store conversation history
conversation_history = []
# ID of the channel you want to send the message to
channel_id = "<SLACK CHANNEL ID>"

try:
    # Call the conversations.history method using the WebClient
    # conversations.history returns the first 100 messages by default
    # These results are paginated, see: https://api.slack.com/methods/conversations.history$pagination
    result = client.conversations_history(channel=channel_id)

    conversation_history = result["messages"]

    results_json=json.dumps(conversation_history)

    messages = jq.compile(".[].text").input_value(conversation_history).all()

    print(messages[0])

    results = open('results.json', 'a')
#    results.write(str(conversation_history))
    json.dump(conversation_history, results)
    results.close()

except SlackApiError as e:
    logger.error("Error creating conversation: {}".format(e))

