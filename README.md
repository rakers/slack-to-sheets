# Slack to Google Sheets Message Copier

## Quickstart

At the moment, only the Node.js version is set up for one-button execution.

### Node.js
Requirements
- Node.js
- npm

1. Clone this repo.
2. Set up a Slack app and obtain a bot token [here](https://api.slack.com/start/quickstart).
    - The required scopes are `channels:history`, `groups:history`, `im:history`, `mpim:history`, and `users:read`.
    - Note the token down for later.
3. Invite the bot to the channel to be copied.
    - Note the channel ID for later, this can be found in the channel details.
4. Note the Google Sheet ID, which is the string of characters in the URL following `gid=`.
5. Replace the `<SLACK BOT TOKEN>`, `<SLACK CHANNEL ID>`, and `<GOOGLE SHEET ID>` in node/secrets.json with the token and IDs obtained in the previous steps.
6. In the node directory, run `npm install`.
7. Run `node main.js`.
    - On the first run, a browser window will open as part of the Google Sheets API auth process. Log in and approve with a user with write access to the Google Sheet being copied to. Future runs will not require a log in as long as the generated token.json remains valid.
    - Required scopes are `https://www.googleapis.com/auth/drive`, `https://www.googleapis.com/auth/drive.file`, and `https://www.googleapis.com/auth/spreadsheets`. The `drive` and `drive.file` scopes are only required for storing and attaching images, which is not currently fully implemented.
8. Messages from the Slack channel should now be appended starting at the first empty cell of the Google Sheet.
 

## Detailed writeup

This application copies messages from Slack into a Google Sheet, using both the Slack API and Google Sheets API. Both APIs are free to use, though they do have per-minute API call limits. This app is unlikely to hit the rate limits on either service, though if a large number of user IDs need to be looked up, it is possible the limit would be reached. This could potentially be mitigated by maintaining a local cache of user ID and username pairs, but this functionality has not yet been added. Slack's API offers libraries for Java, JavaScript, and Python, as well as direct HTTP requests using tools such as curl. Google Sheets' API offers libraries for Java, Node.js (JavaScript), Go, .NET, PHP, Ruby, and direct HTTP requests. Proof of concepts have been written in both Node.js and Python, as both APIs offer libraries in these languages, and no method for obtaining a Google auth token via CLI scripting is readily available in the API docs.

When started, the app will first import the required modules, including libraries for both Slack and Google Sheets' APIs. Secrets such as tokens and sheet IDs are imported from a saved file to prevent saving secrets in the code. The timestamp from the last copied message is imported, if it exists. A few variables are also initialized, including the start of a JSON string for storing parsed responses from Slack to push to Google Sheets.

The app will then request the current message history, using the channel ID and bot token from the secrets file, and the imported timestamp if it exists. The response will include up to 100 messages in JSON format, terminating at the timestamp if supplied. Pagination handling will be needed if there are more than 100 messages since the last timestamp, but this functionality has not yet been added.

If the response is empty, "No new messages" will be printed to the console and the app will terminate. Otherwise, the responses will be looped through. First the body text will be checked for user ID references, and if found, request the name associated with said ID using the slack API, then replace the user ID with the username for readability. The sender's username will also be obtained using the sender's user ID and replaced. The message timestamp will then be converted from a Unix epoch to a 'MM/DD/YYYY - HH:MM:SS' format. This can be easily changed to match existing formatting if needed. The timestamp, sender's username, and message text are then appended to the JSON string. If there are remaining messages in the response, a comma will also be appended. Once the end of the response has been reached, the JSON string is closed.

With the Slack messages parsed and stored in the JSON string, a Google auth token must be obtained. The method to do so has been copied from the Google Sheets API quickstart documentation. If a valid, unexpired auth token exists locally, this step will be invisible to the user. If no valid token is present, a browser window will be opened for the user to manually allow the app access to the Google Sheet, using the sheet ID stored in the secrets file. With a valid auth token, the JSON string is then pushed to be appended to the Google Sheet using the Google Sheets API. Finally, the timestamp of the most recent message is stored to a file, and the app terminates.

Both APIs require the app to be registered and tokens to be generated before use. Instructions for this process can be found in the following documentation pages:
https://api.slack.com/start/quickstart
	Required scopes are:	
		channels:history
		groups:history
		im:history
		mpim:history	
		users:read
https://developers.google.com/sheets/api/quickstart/nodejs
	Requires scopes are:
		Apps Script API: .../auth/spreadsheets
		Apps Script API: .../auth/drive
		Google Sheets API: .../auth/drive.file
	Drive scopes are required for storing and attaching images, which is not currently part of the app but has been manually tested and proven possible.
	Once the token has been created, they must be saved as "credentials.json" and stored in the app's root folder.
	The user who creates the token must also have write access to the Google Sheet, and will also be the user who logs in and approves app access at runtime.

The proof of concept was built without viewing the Ops log, so the formatting may not match, but is easy to adjust.
Pagination for responses of more than 100 replies is also not included in the proof of concept, but certainly possible to automate.
Slack usernames are currently requested every time a user ID is encountered, which is the most likely way this app could hit a per-minute limit. Maintaining and checking a user ID/username cache would likely mitigate the risk of hitting limits, but is not currently part of this app.
Images posted to Slack are currently not copied to the log. Copying images to a Google Drive and including them in the Google Sheet has been manually tested and proved possible.

The only system requirements for this app are the ability to install runtime environments for the language used. Both Node.js and Python have runtimes available for Windows, macOS, and Linux. The system running this app must also be secure for storing tokens, as well as logging in as a user with write access to the Google Sheet.

Aside from obtaining the initial Google auth token, this entire process could be scheduled to run automatically, such as with cron on a Linux system or with Windows Task Scheduler. 
