const fs = require('fs');
let slack = require('./slack_api.js');
let google = require('./google_api.js');
const secrets = require('./secrets.json');
var lastTimestamp = require('./timestamp.json');
let messagesJson;
var userRegex = new RegExp(/<@(U[a-zA-Z0-9]*)>/);

function convertTimeStamp (epochTimeStamp) {
      var d = new Date(epochTimeStamp * 1000);
      var timeStamp = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear() + " - " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
	return timeStamp;
}

async function main () {
	messagesJson = await slack.getMessages(secrets.slackBotToken, secrets.slackChannel, lastTimestamp.timestamp);
	//console.log(messagesJson.length);
	//console.log(messagesJson[4].ts);
	let values = '{ "values": [';
	if (messagesJson.length > 0) {
		for (let i = messagesJson.length; i > 0; i--) {
			while (messagesJson[i-1].text.match(/\<@U[a-zA-Z0-9]*\>/)) {
				var match = userRegex.exec(messagesJson[i-1].text);
				userName = await slack.getUser(secrets.slackBotToken, match[1]);
				messagesJson[i-1].text = messagesJson[i-1].text.replace('<@' + match[1] + '>', userName);
			//} else {
			//	console.log("No matches found");
			}
			//console.log(messagesJson[i-1].text);
			userName = await slack.getUser(secrets.slackBotToken, messagesJson[i-1].user);
			values += '["' + convertTimeStamp(messagesJson[i-1].ts) + '","' + userName  + '","' + messagesJson[i-1].text + '"]';
			if (i - 1 > 0) {
				values += ','
			}
			//console.log(messagesJson[i-1].text + " - " + convertTimeStamp(messagesJson[i-1].ts));
		}
		values += ']}'
		//console.log(values);
		google.updateDoc(secrets.googleSheetId, values);
	


		lastTimestamp.timestamp = messagesJson[0].ts;
		fs.writeFile('./timestamp.json', JSON.stringify(lastTimestamp, null, 2), function writeJson(err) {
			if (err) return console.log(err);
		});
	} else {
		console.log("No new messages");
	}
}
main();
