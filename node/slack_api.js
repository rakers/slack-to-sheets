const { WebClient, LogLevel } = require("@slack/web-api");
//const client = new WebClient(token, {
  //logLevel: LogLevel.DEBUG
//});
async function getMessages (token, channel, timestamp) {
	const client = new WebClient(token, {
		//logLevel: LogLevel.DEBUG
	});
	let conversationHistory;
	let channelId = channel;
	const result = await client.conversations.history({ channel: channelId, oldest: timestamp });
	conversationHistory = result.messages;
	return conversationHistory;
}

async function getUser (token, userId) {
	const client = new WebClient(token, {
		//logLevel: LogLevel.DEBUG
	});
	const result = await client.users.info({ user: userId });
	return result.user.real_name;
}

module.exports = { getMessages, getUser }
